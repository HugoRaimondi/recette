package com.recette.recette;

import com.recette.recette.controller.RecetteController;
import com.recette.recette.dao.RecetteDAO;
import com.recette.recette.model.Recette;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@Profile("int")
@SpringBootTest
public class RecetteRepositoryIntegrationTest {
    @InjectMocks
    private RecetteController recetteController;

    @Mock
    private RecetteDAO recetteDAO;

    @Test
    public void create() {

        Recette recette = new Recette();
        recette.setTitre("name");
        recette.setRecette("recette");

        Recette mockNewRecette = new Recette();
        mockNewRecette.setId(1l);

        Mockito.when(recetteDAO.save(recette)).thenReturn(mockNewRecette);

        //Act
        Recette newRecette = recetteDAO.save(recette);

        //Assert
        assertNotNull(newRecette);
        assertNotNull(newRecette.getId());
    }


    @Test
    public void findAll() {
        //Arrange
        List<Recette> mockRecettes = new ArrayList<Recette>();
        Recette mockRecette1 = new Recette();
        mockRecette1.setId(1l);

        Recette mockRecette2 = new Recette();
        mockRecette2.setId(2l);

        mockRecettes.add(mockRecette1);
        mockRecettes.add(mockRecette2);

        Mockito.when(recetteDAO.findAll()).thenReturn((ArrayList<Recette>) mockRecettes);

        //Act
        List<Recette> recettes = recetteController.findAll();

        //Assert
        assertNotNull(recettes);
    }

    @Test
    public void delete() {
        //Arrange
        Long recetteId = 1l;

        //Act
        Boolean success = recetteController.delete(recetteId);

        //Assert
        assertTrue(success);
    }
}
