package com.recette.recette.controller;

import com.recette.recette.dao.RecetteDAO;
import com.recette.recette.model.Recette;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("api/recettes")
@CrossOrigin(origins = "http://localhost:4200")
public class RecetteController{

    private RecetteDAO recetteDAO;

    @Autowired
    public RecetteController(RecetteDAO recetteDAO) {
        this.recetteDAO = recetteDAO;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/hello", produces = "application/json")
    public ResponseEntity<String> hello() {
        System.out.println("Hit me!");
        return new ResponseEntity<String>("Hello, you!", HttpStatus.OK);
    }
    /**
     * findAll
     *
     * @return all recettes
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/findAll")
    public ArrayList<Recette> findAll() {
        return this.recetteDAO.findAll();
    }

    /**
     * create
     *
     * @param recetteModel recetteModel
     * @return recetteModel
     */
    @PostMapping("/")
    public Recette create(@RequestBody @Valid Recette recetteModel) {
        return this.recetteDAO.save(recetteModel);
    }

    /**
     * delete
     *
     * @param id id
     */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Long id) {
        this.recetteDAO.deleteById(id);

        return true;
    }
}
