package com.recette.recette.dao;

import com.recette.recette.model.Recette;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RecetteDAO extends JpaRepository<Recette, Integer> {

    ArrayList<Recette> findAll();

    Recette findById(long id);

    Recette save(Recette recette);

    void deleteById(Long id);
}