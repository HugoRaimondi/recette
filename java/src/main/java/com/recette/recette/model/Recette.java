package com.recette.recette.model;

import javax.persistence.*;

@Entity
@Table(name = "recette")
public class Recette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String titre;

    private String recette;

    public long getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getRecette() {
        return recette;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setRecette(String recette) {
        this.recette = recette;
    }
}
