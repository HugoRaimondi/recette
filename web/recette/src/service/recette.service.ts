import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Recette } from 'src/model/recette.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecetteService {
  private baseUrl = (environment.url+"/api/recettes");

  constructor(private http: HttpClient) { }

  findAll(): Observable<any> {
    return this.http.get(this.baseUrl + "/findAll");
  }

  save(recette: Recette): Observable<any> {
    return this.http.post(this.baseUrl + "/", recette);
  }

  delete(id: Number): Observable<any> {
    return this.http.delete(this.baseUrl + "/" + id);
  }
}
