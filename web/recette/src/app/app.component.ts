import { Component, ViewChild } from '@angular/core';
import { RecetteService } from 'src/service/recette.service';
import { Recette } from 'src/model/recette.model';
import {MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { faFilePdf, faTimes } from '@fortawesome/free-solid-svg-icons';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  displayedColumns: Array<string>;
  faFilePdf = faFilePdf;
  faTimes = faTimes;
  recettes: Array<Recette>;
  dataSource: MatTableDataSource<Recette>;
  selection = new SelectionModel<Recette>(true, []);

  constructor(public recetteService: RecetteService, private _snackBar: MatSnackBar) { }

 sort: MatSort;
  paginator: MatPaginator;

  ngOnInit() {
    this.displayedColumns = ["select", "title", "text"];
    this.getAllRecette();
  }

  getAllRecette() {
    this.recetteService.findAll().subscribe((recettes: Array<Recette>) => {
      this.recettes = recettes;
      this.initDataSource();
    });
  }

  initDataSource() {
    this.dataSource = new MatTableDataSource<Recette>(this.recettes);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    console.log(this.selection.selected)
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  download(){
    var doc = new jsPDF();
    if(this.selection.selected.length >= 1){this.selection.selected.forEach(recette => {
        doc.text(20, 20, recette.titre);
        doc.text(20, 30, recette.recette);
        if(this.selection.selected.length > 1){doc.addPage();}
      });

        // Save the PDF
        doc.save('Recettes, nb: '+this.selection.selected.length+'.pdf');
      }else this._snackBar.open("Vous n'avez sélectionné aucune recette", "fermer", {
          duration: 2000,
          panelClass: ['red-snackbar']
        });
      }
  }

