export class Recette {
    constructor(
        public id: number,
        public titre: string,
        public recette: string) { }
}
